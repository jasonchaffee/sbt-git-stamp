import SonatypeKeys._

sonatypeSettings

/* basic project info */
name := "sbt-git-stamp"

organization := "com.atlassian.labs"

sbtPlugin := true

description := "An SBT plugin that will stamp the MANIFEST.MF file in the output artifact with some basic git information."

homepage := Some(url("https://bitbucket.org/pkaeding/sbt-git-stamp"))

licenses := Seq("Apache 2.0" → url("http://www.apache.org/licenses/LICENSE-2.0"))

startYear := Some(2013)

/* scala versions and options */
scalaVersion := "2.10.1"

crossBuildingSettings

CrossBuilding.crossSbtVersions := Seq("0.11.2", "0.11.3", "0.12", "0.13")

offline := false

scalacOptions ++= Seq("-deprecation", "-unchecked")

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

/* dependencies */
libraryDependencies ++= Seq (
  "com.github.nscala-time" %% "nscala-time" % "0.2.0",
  "org.eclipse.jgit" % "org.eclipse.jgit" % "2.2.0.201212191850-r"
)

/* you may need these repos */
resolvers ++= Seq(
  // Resolvers.sonatypeRepo("snapshots")
  // Resolvers.typesafeIvyRepo("snapshots")
  // Resolvers.typesafeIvyRepo("releases")
  // Resolvers.typesafeRepo("releases")
  // Resolvers.typesafeRepo("snapshots")
  // JavaNet2Repository,
  // JavaNet1Repository
)

/* sbt behavior */
logLevel in compile := Level.Warn

traceLevel := 5

releaseSettings

publishMavenStyle := true

publishTo <<= version { (v: String) =>
  val nexus = "https://oss.sonatype.org/"
  if (v.trim.endsWith("SNAPSHOT"))
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases"  at nexus + "service/local/staging/deploy/maven2")
}

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <scm>
    <url>git@bitbucket.org:pkaeding/sbt-git-stamp.git</url>
    <connection>scm:git:git@bitbucket.org:pkaeding/sbt-git-stamp.git</connection>
  </scm>
  <developers>
    <developer>
      <id>pkaeding</id>
      <name>Patrick Kaeding</name>
      <email>pkaeding@atlassian.com</email>
      <url>http://kaeding.name</url>
    </developer>
  </developers>
)

// Josh Suereth's step-by-step guide to publishing on sonatype
// httpcom://www.scala-sbt.org/using_sonatype.html

